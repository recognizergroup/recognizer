package repository

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/redis/go-redis/v9"
	"net/url"
	. "recognizer/internal/app/repository"
	. "recognizer/pkg/common"
	"time"
)

/*
TODO everything in this file should eventually be replaced with a scalable db + redis but it should still function
	more or less the same way
*/

const (
	RouteKeyPattern = "ROUTE *"
	TraceKeyPattern = "TRACE SPAN  *"
	SpanKeyPattern  = "SPAN  *"
)

func GetRepository() *redis.Client {

	return RedisClient
}

func newTraceHash(s string) (TraceHash, error) {
	th := TraceHash{
		Values: make([]SpanHash, 0),
	}
	err := json.Unmarshal([]byte(s), &th)
	return th, err
}

func LearnTrace(key string, traceHashS string, rt *ReducedTrace) {
	th, mErr := newTraceHash(traceHashS)
	if mErr != nil {
		panic(mErr)
	}
	insertTraceRecord(key, &TraceRecord{
		History: History{
			Timestamps: []time.Time{th.Values[0].Request.TimeStamp, th.Values[0].Response.TimeStamp},
		},
		ReducedTrace: rt,
	})
	parentSpan := rt.GetParent()
	if parentSpan == nil {
		panic(fmt.Sprintf("cannot find parent span in %v", rt))
	}

	InsertRoute(key, parentSpan.Request.URL)

	for _, sh := range th.Values {
		key := sh.GetKey()
		rs := rt.Map[key]
		if !recognizeSpan(sh) {
			learnSpan(sh, rs)
		}
	}
}

func SearchTrace(v url.Values) []*TraceRecord {
	trs := make([]*TraceRecord, 0)
	if v.Has("Hash") {
		if tr := getTraceRecord(v.Get("Hash")); tr != nil {
			trs = append(trs, tr)
			return trs
		}
	}
	keys := GetRouteKeys(v.Get("Route")) // TODO should Route be required if Hash is not provided?

	// TODO can this part be done within redis? search by a created_at timestamp perhaps? create a separate timeline record?
	//  and we can preload the spans associated with a trace automagically so subsequent span searches within the trace are quick
	startS, endS := v.Get("Start"), v.Get("End")
	startT, endT := ParseTimes(startS, endS)
	if len(keys) == 0 {
		keys = RedisClient.Keys(context.Background(), TraceKeyPattern).Val()
	}
	for _, key := range keys {
		tr := getTraceRecord(key)
		var reqTS time.Time
		for index, timestamp := range tr.Timestamps {
			if index%2 == 0 {
				reqTS = timestamp
				continue
			}
			if IsTimely(timestamp, startT, endT) {
				retTR := &TraceRecord{
					History:      History{Timestamps: []time.Time{reqTS}},
					ReducedTrace: tr.ReducedTrace,
				}
				trs = append(trs, retTR)
			}
		}
	}

	return trs
}

func SearchSpan(v url.Values) []*SpanRecord {
	srs := make([]*SpanRecord, 0)
	if v.Has("Hash") {
		if sr := getSpanRecord(v.Get("Hash")); sr != nil {
			srs = append(srs, sr)
			return srs
		}
	}
	keys := GetRouteKeys(v.Get("Route"))
	if len(keys) == 0 {
		keys = RedisClient.Keys(context.Background(), SpanKeyPattern).Val()
	}

	startS, endS := v.Get("Start"), v.Get("End")
	startT, endT := ParseTimes(startS, endS)
	for _, key := range keys {
		sr := getSpanRecord(key)
		if sr == nil {
			panic(fmt.Sprintf("redis missing record %v", key))
		}
		for index, timestamp := range sr.Timestamps {
			if index%2 == 0 {
				continue
			}
			if IsTimely(timestamp, startT, endT) {
				retSR := &SpanRecord{
					History:     History{Timestamps: []time.Time{timestamp}},
					ReducedSpan: sr.ReducedSpan,
				}
				srs = append(srs, retSR)
			}
		}
	}

	// TODO time-box
	return srs
}

func getSpanRecord(key string) *SpanRecord {
	resp := RedisClient.Get(context.Background(), key).Val()
	if resp == "" {
		return nil
	}
	sr := new(SpanRecord)
	err := json.Unmarshal([]byte(resp), sr)
	if err != nil {
		panic(err)
	}
	return sr
}

func getTraceRecord(key string) *TraceRecord {
	resp := RedisClient.Get(context.Background(), key).Val()
	if resp == "" {
		return nil
	}
	tr := new(TraceRecord)
	umErr := json.Unmarshal([]byte(resp), tr)
	if umErr != nil {
		panic(umErr)
	}
	return tr
}

func insertTraceRecord(key string, tr *TraceRecord) *TraceRecord {
	m, err := json.Marshal(*tr)
	if err != nil {
		panic(err)
	}
	RedisClient.Set(context.Background(), key, m, 0)
	return tr
}

func bumpTraceIndex(key string, reqTime, resTime time.Time) {
	tr := getTraceRecord(key)
	tr.Update(reqTime, resTime)
	insertTraceRecord(key, tr)
}
func RecognizeTrace(key string, traceS string) (bool, error) {
	tr := getTraceRecord(key)
	if tr != nil {
		th, mErr := newTraceHash(traceS)
		if mErr == nil {
			bumpTraceIndex(key, th.Values[0].Request.TimeStamp, th.Values[0].Response.TimeStamp)
		}
		return true, mErr
	}
	return false, nil
}

func bumpSpanIndex(key string, reqTime, resTime time.Time) {
	sr := getSpanRecord(key)
	sr.Update(reqTime, resTime)
	insertSpanRecord(key, sr)
}

func learnSpan(sh SpanHash, rs *ReducedSpan) {
	key := sh.GetKey()
	sr := SpanRecord{
		ReducedSpan: rs,
		History: History{
			Timestamps: make([]time.Time, 0),
		},
	}
	insertSpanRecord(key, &sr)
	InsertRoute(key, rs.Request.URL)

	bumpSpanIndex(key, rs.Request.TimeStamp, rs.Response.TimeStamp)
}
func recognizeSpan(sh SpanHash) bool {
	key := sh.GetKey()
	sr := getSpanRecord(key)
	if sr != nil {
		bumpSpanIndex(key, sh.Request.TimeStamp, sh.Response.TimeStamp)
	}
	return sr != nil
}

func insertSpanRecord(key string, sr *SpanRecord) {
	b, err := json.Marshal(*sr)
	if err != nil {
		panic(err)
	}
	RedisClient.Set(context.Background(), key, b, 0)
}
