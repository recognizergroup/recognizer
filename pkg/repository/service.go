package repository

import (
	"encoding/json"
	"io"
	"net/http"
	. "recognizer/pkg/common"
)

func writeResponse(w http.ResponseWriter, obj interface{}, status int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	s, _ := json.Marshal(obj) // TODO should we swallow error here?
	_, wErr := io.WriteString(w, string(s))
	if wErr != nil {
		panic(wErr)
	}
}

type TraceResponseShape struct {
	Traces []*TraceRecord `json:"traces"`
}

type SpanResponseShape struct {
	Spans []*SpanRecord `json:"spans"`
}

func RouteRequest(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		query := r.URL.Query()
		route := r.URL.Path
		switch route {
		case "/trace":
			trs := TraceResponseShape{
				Traces: SearchTrace(query),
			}
			writeResponse(w, trs, 200)
			break
		case "/span":
			srs := SpanResponseShape{
				Spans: SearchSpan(query),
			}
			writeResponse(w, srs, 200)
			break
		}

	} else if r.Method == "POST" {
		traceHashS := r.Header.Get(TraceMetaHeaderKey)
		key := r.Header.Get(TraceHashHeaderKey)

		if ok, mErr := RecognizeTrace(key, traceHashS); ok {
			if mErr != nil {
				panic(mErr)
			}
			writeResponse(w, nil, http.StatusAlreadyReported)
		} else {
			bodyBytes, readErr := io.ReadAll(r.Body)
			if readErr != nil {
				panic(readErr)
			}
			rt := &ReducedTrace{}
			umErr := json.Unmarshal(bodyBytes, rt)
			if umErr != nil {
				panic(umErr)
			}
			LearnTrace(key, traceHashS, rt)
			writeResponse(w, nil, http.StatusOK)
		}
	}
}
