package reducer

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"github.com/PaesslerAG/jsonpath"
	"github.com/antchfx/xmlquery"
	"io"
	"net/http"
	"os"
	"path"
	"path/filepath"
	. "recognizer/pkg/common"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var (
	Masks   = make(map[int]Mask)
	Filters = make(map[string]Filter)
)

func getConfigs(fileName string) [][]string {
	wd, err := os.Getwd()
	if err != nil {
		panic(err) // TODO this will go away when we make persistent
	}
	filePath := path.Join(wd, "configs", fileName)
	f, osErr := os.Open(filePath)
	if osErr != nil {
		filePath = path.Join(filepath.Dir(wd), "configs", fileName)
		f, osErr = os.Open(filePath)
		if osErr != nil {
			panic(osErr)
		}
	}
	r := csv.NewReader(f)
	records, readErr := r.ReadAll()
	if readErr != nil {
		panic(readErr)
	}
	return records
}

func reduceRecording(h http.Header, b *bytes.Buffer, masks *MaskSignature, ts time.Time) ReducedRecording {
	reducedHeader := reduceHeader(h, masks)
	r := ReducedRecording{
		Header:    reducedHeader,
		TimeStamp: ts,
	}
	if b != nil {
		r.Body.Ptr = reduceBody(b, masks)
	}
	return r
}

// ReduceRequest returns the MaskSignature for the response mask
func ReduceRequest(recReq RecordedRequest) (ReducedRequest, *MaskSignature) {
	reqMask, resMask := getMatchingMasks(recReq)
	reducedRecording := reduceRecording(recReq.Header, recReq.Body, &reqMask, recReq.TimeStamp)
	reducedRequest := ReducedRequest{
		Request: Request{
			URL:    recReq.URL,
			Method: recReq.Method,
		},
		ReducedRecording: reducedRecording,
	}
	return reducedRequest, &resMask
}

func ReduceResponse(recRes RecordedResponse, ms *MaskSignature) ReducedResponse {
	reducedResponse := ReducedResponse{}
	reducedRec := reduceRecording(recRes.Header, recRes.Body, ms, recRes.TimeStamp)
	reducedResponse.ReducedRecording = reducedRec

	if len(recRes.Errors) > 0 {
		errs := reduceErrors(recRes.Errors, ms)
		reducedResponse.Errors = errs
	}

	return reducedResponse
}

// getMatchingMasks returns a pointer to a MaskSignature for Request and Response
//
//	it is used to match the request route to set of MaskSignature, one for header, one for body
func getMatchingMasks(recReq RecordedRequest) (MaskSignature, MaskSignature) {
	filters := loadFilters()
	for _, filter := range filters {
		switch filter.MatchPatternType {
		case "raw":
			if filter.MatchPattern == recReq.Method+" "+recReq.URL {
				return filter.RequestMaskSignature, filter.ResponseMaskSignature
			}
		case "regexp":
			re := regexp.MustCompile(filter.MatchPattern)
			result := re.Find([]byte(recReq.URL))
			if result != nil {
				return filter.RequestMaskSignature, filter.ResponseMaskSignature
			}
		}
	}
	return MaskSignature{}, MaskSignature{}
}

// reduceHeader takes a Header and a pointer to MaskSignature and returns a new Header
// to which the masks in MaskSignature.HeaderMaskIds are applied. if any of the
// masks in that array do NOT result in a change to the new Header, then that MaskId
// is removed from MaskSignature.HeaderMaskIds so that it does not become a part of the
// reduced header's signature
func reduceHeader(h http.Header, ms *MaskSignature) http.Header {
	loadMasks()
	newH := h.Clone()
	var matchingMaskIds []int

	for _, maskId := range ms.HeaderMaskIds {
		mask := Masks[maskId]
		switch mask.MatchPatternType {
		case "raw": // matches header field directly
			if h.Get(mask.MatchPattern) != mask.ReplaceString {
				matchingMaskIds = append(matchingMaskIds, maskId)
			}
			newH.Set(mask.MatchPattern, mask.ReplaceString)
			break
		case "regex":
			re := regexp.MustCompile(mask.MatchPattern)
			for k := range newH {
				if re.Find([]byte(k)) != nil {
					newH.Set(k, mask.ReplaceString)
				}
			}
			matchingMaskIds = append(matchingMaskIds, maskId)
			break
		}
	}

	ms.HeaderMaskIds = matchingMaskIds

	return newH
}

func reduceBody(b *bytes.Buffer, ms *MaskSignature) *[]byte {
	loadMasks()
	bodyBytes, readErr := io.ReadAll(b)
	if readErr != nil {
		panic(readErr)
	}
	b = bytes.NewBuffer(bodyBytes)
	var (
		matchingMaskIds []int
	)

	for _, maskId := range ms.BodyMaskIds {
		mask := Masks[maskId]
		switch mask.MatchPatternType {
		case "raw": // calls  body raw text directly
			if strings.Contains(string(bodyBytes), mask.MatchPattern) {
				bodyBytes = []byte(strings.Replace(string(bodyBytes), mask.MatchPattern, mask.ReplaceString, -1))

				matchingMaskIds = append(matchingMaskIds, maskId)
			}
		case "regex":
			re := regexp.MustCompile(mask.MatchPattern)
			if re.Find(bodyBytes) != nil {
				bodyBytes = []byte(re.ReplaceAllString(string(bodyBytes), mask.ReplaceString))
				matchingMaskIds = append(matchingMaskIds, maskId)
			}
		case "json":
			v := interface{}(nil)
			if err := json.Unmarshal(bodyBytes, &v); err != nil {
				// handle error
			}
			// TODO this exposes a gap - we need a json stream-parser where we can mask as we read once instead of converting back and forth between json and interface{}
			// TODO also smaller gap, what to do if more than one result?
			result, _ := jsonpath.Get(mask.MatchPattern, v)
			replaceBytes, _ := json.Marshal(result)
			bodyBytes = []byte(strings.Replace(string(bodyBytes), string(replaceBytes), mask.ReplaceString, -1))
		case "xml":
			doc, parseErr := xmlquery.Parse(strings.NewReader(string(bodyBytes)))
			if parseErr != nil {
				panic(parseErr)
			}
			resultList, queryErr := xmlquery.QueryAll(doc, mask.MatchPattern)
			if queryErr != nil {
				panic(queryErr)
			}
			for _, result := range resultList {
				result.FirstChild.Data = mask.ReplaceString
			}
			bodyBytes = []byte(doc.OutputXML(true))
		}

	}

	ms.HeaderMaskIds = matchingMaskIds

	return &bodyBytes
}

func reduceErrors(errs []error, ms *MaskSignature) []error {
	reducedErrors := make([]error, len(errs))
	var usedMaskIds []int
	for index, e := range errs {
		for _, maskId := range ms.ErrorMaskIds {
			mask := Masks[maskId]
			switch mask.MatchPatternType {
			case "regex":
				errorS := e.Error()
				re := regexp.MustCompile(mask.MatchPattern)
				if re.Find([]byte(errorS)) != nil {
					newS := re.ReplaceAllString(errorS, mask.ReplaceString)
					newE := ReducedError{Message: newS}
					reducedErrors[index] = newE
					usedMaskIds = append(usedMaskIds, maskId)
					break
				}
			}
		}
	}
	ms.ErrorMaskIds = usedMaskIds
	return errs
}
func atoiSplit(s string) []int {
	a := strings.Split(s, ";")
	retVal := make([]int, len(a))
	for i, c := range a {
		if c == "" {
			continue
		}
		val, scErr := strconv.Atoi(c)
		if scErr != nil {
			panic(scErr)
		}
		retVal[i] = val
	}
	return retVal
}

func loadFilters() map[string]Filter {
	if len(Filters) == 0 {
		for index, record := range getConfigs("filters.csv") {
			if index == 0 {
				continue // skipping headers
			}
			if len(record) < 8 { // TODO find a better way
				panic("invalid record")
			}
			Filters[record[0]] = Filter{
				Pattern: Pattern{
					MatchPattern:     record[0],
					MatchPatternType: record[1],
				},
				RequestMaskSignature: MaskSignature{
					HeaderMaskIds: atoiSplit(record[2]),
					BodyMaskIds:   atoiSplit(record[3]),
				},
				ResponseMaskSignature: MaskSignature{
					HeaderMaskIds: atoiSplit(record[4]),
					BodyMaskIds:   atoiSplit(record[5]),
					ErrorMaskIds:  atoiSplit(record[6]),
				},
			}
		}
	}
	return Filters
}

func loadMasks() map[int]Mask {
	if len(Masks) == 0 {
		for i, record := range getConfigs("masks.csv") {
			if i == 0 {
				continue // skipping headers
			}
			if len(record) < 5 {
				panic("invalid record")
			}
			j, scErr := strconv.Atoi(record[0])
			if scErr != nil {
				panic(scErr)
			}
			Masks[j] = Mask{
				Pattern: Pattern{
					MatchPattern:     record[2],
					MatchPatternType: record[1],
					Description:      record[4],
				},
				ReplaceString: record[3],
			}
		}
	}
	return Masks
}
