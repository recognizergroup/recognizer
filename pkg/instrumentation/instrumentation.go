package instrumentation

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	. "recognizer/pkg/common"
	. "recognizer/pkg/instrumentation/collector"
	. "recognizer/pkg/instrumentation/reducer"
	"time"
)

var (
	LearningMode = true
	trace        = &Trace{}
)

func ClearTrace(finalArgs ...interface{}) {
	if LearningMode { // TODO
		HandleTrace(trace, finalArgs...)
	}
	//trace.Spans = nil
}

// instrumentRequestBody returns a pointer to a buffer that will get populated if r.Body is ever read
func instrumentRequestBody(r *http.Request) *bytes.Buffer {
	instrumentedBody, buf := getInstrumentedBodies(r.Body)
	r.Body = instrumentedBody
	return buf
}

// instrumentBody returns an io.TeeReader and a synchronously-bound buffer. Reads on the TeeReader will
// Write to that buffer as a side effect for concurrent collection and testing
func getInstrumentedBodies(body io.ReadCloser) (io.ReadCloser, *bytes.Buffer) {
	buf := bytes.NewBuffer([]byte(``))
	tR := io.TeeReader(body, buf)
	nC := io.NopCloser(tR)
	return nC, buf
}

// instrumentResponseBody returns a pointer to a buffer that will get populated if r.Body is ever read
func instrumentResponseBody(r *http.Response) *bytes.Buffer {
	instrumentedBody, buf := getInstrumentedBodies(r.Body)
	r.Body = instrumentedBody
	return buf
}

func newRecordingFromReq(r *http.Request) RecordedRequest {
	rr := RecordedRequest{
		Request: Request{
			Method: r.Method,
			URL:    r.URL.String(),
		},
		Recording: Recording{
			Header:    r.Header,
			TimeStamp: time.Now(),
		},
	}

	if r.Method == "POST" {
		collectorBody := instrumentRequestBody(r)
		rr.Recording.Body = collectorBody
	}

	return rr
}

func newGetRecording(newURL string) RecordedRequest {
	return RecordedRequest{
		Request: Request{
			Method: http.MethodGet,
			URL:    newURL,
		},
		Recording: Recording{
			TimeStamp: time.Now(),
			Header:    http.Header{},
		},
	}
}

func addSpan(rec RecordedRequest) int {
	trace.Spans = append(trace.Spans, Span{Request: rec})
	return len(trace.Spans) - 1
}

// called whenever a sut request is received or a dependency call is made to append
// the corresponding span recording to the currently active trace
func addSpanFromRequest(req *http.Request) int {
	recording := newRecordingFromReq(req)
	return addSpan(recording)
}

func UpdateSpan(index int, r *http.Response) {
	thisSpan := trace.Spans[index]
	thisSpan.Response = RecordedResponse{
		Recording: Recording{
			Header:    r.Header,
			TimeStamp: time.Now(),
		},
	}
	if r.Body != nil {
		collectorBodyBuffer := instrumentResponseBody(r)
		thisSpan.Response.Recording.Body = collectorBodyBuffer
	}
	trace.Spans[index] = thisSpan
}

func AddErrors(index int, errs ...error) {
	trace.Spans[index].Response = RecordedResponse{
		Errors: errs, // TODO serialize? stringify?
		Recording: Recording{
			TimeStamp: time.Now(),
		},
	}
}

// HandleTrace will start the collection process. any further shutdown activity by the service will be
//
//	unrecorded!!!
func HandleTrace(trace *Trace, finalArgs ...interface{}) {
	th := TraceHash{}
	rt := ReducedTrace{
		Map:  make(map[string]*ReducedSpan),
		Keys: make([]string, 0),
	}
	var finalKey string
	for index, span := range trace.Spans {
		if index == 0 {
			span.Response.TimeStamp = time.Now()
			if len(finalArgs) > 0 {
				span.Response.Header = finalArgs[0].(http.Header)
			}
			if len(finalArgs) > 1 {
				span.Response.Body = bytes.NewBuffer([]byte(finalArgs[1].(string)))
			}
		}
		reducedRequest, responseMaskSignature := ReduceRequest(span.Request)
		reducedResponse := ReduceResponse(span.Response, responseMaskSignature)
		// TODO should we separate the hashing into a second for loop?
		//  could more easily enable on-site replay; maybe make it lazy and use goroutines?
		reqHash := Hash(reducedRequest.ReducedRecording)
		resHash := Hash(reducedResponse.ReducedRecording, reducedResponse.Errors...)
		sh := SpanHash{Request: reqHash, Response: resHash}
		th.Values = append(th.Values, sh)
		rs := &ReducedSpan{
			Request:  reducedRequest,
			Response: reducedResponse,
		}
		key := rt.InsertSpan(rs, sh)
		finalKey += key
	}
	traceHashSerialized, headerMarshalErr := json.Marshal(th)

	if headerMarshalErr != nil {
		panic(headerMarshalErr)
	}
	finalKey = "TRACE " + finalKey
	rt.Key = finalKey
	reducedTraceSerialized, bodyMarshalErr := json.Marshal(rt)
	if bodyMarshalErr != nil {
		panic(bodyMarshalErr)
	}

	ShipTrace(finalKey, traceHashSerialized, reducedTraceSerialized)
	trace.Spans = nil
}

// InstrumentRequest called to record header and body of incoming/outgoing http requests to file
func InstrumentRequest(r *http.Request) int {
	index := addSpanFromRequest(r)
	return index
}

func InstrumentAnything[T json.Marshaler, K json.Marshaler](phunk func(*T) K, v *T) K {
	b, e := json.Marshal(v)
	if e != nil {
		panic(e) // TODO
	}
	spanReq := &http.Request{
		Body: io.NopCloser(bytes.NewReader(b)),
	}
	index := addSpanFromRequest(spanReq)

	resp := phunk(v)

	respB, respErr := json.Marshal(resp)
	if respErr != nil {
		panic(respErr) //TODO
	}

	spanRes := &http.Response{
		Body: io.NopCloser(bytes.NewReader(respB)),
	}
	UpdateSpan(index, spanRes)

	return resp
}

// Error to be used in SUT to indicate terminal error state of request handling and ship
// trace recording out while responding to client
func Error(w http.ResponseWriter, errs ...error) {
	if len(errs) > 0 {
		AddErrors(0, errs...)
	}

	w.WriteHeader(http.StatusInternalServerError)
	body := "Internal Error"
	_, err := io.WriteString(w, body)
	if err != nil {
		go HandleTrace(trace, w.Header(), err.Error())
	} else {
		go HandleTrace(trace, w.Header(), body)
	}
}

type HeaderVal struct {
	Key   string
	Value string
}

// InstrumentedGet example of instrumented request func to wrap http.Get
func InstrumentedGet(newURL string, headerVals ...HeaderVal) (*http.Response, error) {
	nr := newGetRecording(newURL)

	headerMap := make(http.Header)
	for _, hv := range headerVals {
		headerMap[hv.Key] = []string{hv.Value}
	}
	newNewUrl, _ := url.Parse(newURL)

	getReq := &http.Request{
		Method: http.MethodGet,
		URL:    newNewUrl,
		Header: headerMap,
	}
	nr.Header = headerMap
	index := addSpan(nr)

	resp, err := http.DefaultClient.Do(getReq)
	if resp != nil {
		UpdateSpan(index, resp)
	}
	return resp, err
}
