package collector

import (
	"bytes"
	"encoding/json"
	"fmt"
	"hash/fnv"
	"net/http"
	. "recognizer/pkg/common"
	"strconv"
)

var (
	recognizerAddr = "http://localhost:5555"
)

func getHashString(v any) string {
	b, marshalErr := json.Marshal(v)
	if marshalErr != nil {
		panic(marshalErr)
	}
	h := fnv.New32a() // TODO make this configurable later
	_, writeErr := h.Write(b)
	if writeErr != nil {
		panic(writeErr)
	}
	return strconv.FormatUint(uint64(h.Sum32()), 16)
}

func Hash(recording ReducedRecording, errors ...error) HashValue {
	headerHash := getHashString(recording.Header)
	hv := HashValue{HeaderVal: headerHash, TimeStamp: recording.TimeStamp}
	if recording.Body.Ptr != nil {
		b := getHashString(recording.GetBody())
		hv.BodyVal = b
	}

	if len(errors) > 0 {
		c := getHashString(errors)
		hv.ErrorVal = c
	}

	return hv
}

func ShipTrace(traceHash string, traceMeta []byte, traceRecordingBody []byte) {
	req, nrErr := http.NewRequest("POST", recognizerAddr, bytes.NewBuffer(traceRecordingBody))
	if nrErr != nil {
		panic(nrErr)
	}
	req.Header.Add(TraceMetaHeaderKey, string(traceMeta))
	req.Header.Add(TraceHashHeaderKey, traceHash)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		if resp != nil {
			fmt.Printf("response status code: %d\n", resp.StatusCode)
		}
		panic(err) // TODO put in retry? generate an error on failure?
	}
}
