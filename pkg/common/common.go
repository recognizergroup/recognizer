// Package pkg contains public and private members that form the interface of the span repository and provide
// an API for instrumenting an HTTP golang service
package common

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"strconv"
	"time"
)

const (
	TraceHashHeaderKey = "Trace-Hash"
	TraceMetaHeaderKey = "Trace-Meta"
)

type TraceHash struct {
	Values []SpanHash
}

type Recording struct {
	Body      *bytes.Buffer
	Header    http.Header
	TimeStamp time.Time
}

// Replay - does this need to TeeRead again so buffer never gets consumed or closed?
func (r Recording) Replay() string {
	if r.Body == nil {
		return ""
	}
	body, err := io.ReadAll(r.Body)
	if err == nil {
		return string(body)
	} else {
		return ""
	}

}

type ReducedRecording struct {
	Body      BodyBytesPtr // we're making this different to avoid passing body in full; is this necessary?
	Header    http.Header
	TimeStamp time.Time
	MaskSignature
}

func (rr ReducedRecording) GetBody() []byte {
	if rr.Body.Ptr == nil {
		return nil
	}
	return *rr.Body.Ptr
}

type Request struct {
	Method string
	URL    string
}

type RecordedRequest struct {
	Recording
	Request
}

type RecordedResponse struct {
	Recording
	Errors []error
}

type ReducedRequest struct {
	Request
	ReducedRecording
}

type ReducedResponse struct {
	ReducedRecording
	Errors []error
}

type Span struct {
	Request  RecordedRequest
	Response RecordedResponse
}

type Trace struct {
	Spans []Span // The last Span is always the parent
}

type Pattern struct {
	MatchPatternType string
	MatchPattern     string
	Description      string
}

// Filter loaded from dB and contains  various properties of the Request including
// Body to determine which series of Mask to apply to the Header and Body
type Filter struct {
	Pattern
	RequestMaskSignature  MaskSignature
	ResponseMaskSignature MaskSignature
	Description           string // friendly name for this dependency type or category or domain
}

type MaskSignature struct {
	HeaderMaskIds []int
	BodyMaskIds   []int
	ErrorMaskIds  []int
}

type ReducedError struct {
	Message string
}

func (e ReducedError) Error() string {
	return e.Message
}

type Mask struct {
	Pattern
	ReplaceString string
}

type HashValue struct {
	HeaderVal string
	BodyVal   string
	ErrorVal  string
	TimeStamp time.Time
}

type ReducedSpan struct {
	Key      string
	Request  ReducedRequest
	Response ReducedResponse
}

type ReducedTrace struct {
	Key  string
	Map  map[string]*ReducedSpan
	Keys []string
}

// InsertSpan should ALWAYS be invoked in the order of the spans in the original trace
// returns the hash string for the inserted SpanHash
func (rt *ReducedTrace) InsertSpan(rs *ReducedSpan, sh SpanHash) string {
	key := sh.GetKey()
	rs.Key = key
	rt.Map[key] = rs
	rt.Keys = append(rt.Keys, key)
	return key
}

func (rt *ReducedTrace) GetParent() *ReducedSpan {
	if len(rt.Keys) < 1 {
		return nil
	}
	rs := rt.Map[rt.Keys[0]]
	return rs
}

type SpanHash struct {
	Request  HashValue
	Response HashValue
}

func (sh SpanHash) GetKey() string {
	reqHV := HashValue{
		HeaderVal: sh.Request.HeaderVal,
		BodyVal:   sh.Request.BodyVal,
	}
	resHV := HashValue{
		HeaderVal: sh.Response.HeaderVal,
		BodyVal:   sh.Response.BodyVal,
		ErrorVal:  sh.Response.ErrorVal,
	}
	reqB, reqJSONErr := json.Marshal(reqHV)
	resB, resJSONErr := json.Marshal(resHV)
	if reqJSONErr != nil || resJSONErr != nil {
		if reqJSONErr != nil {
			panic(reqJSONErr)
		}
		panic(resJSONErr)
	}
	keyS := "SPAN  " + string(reqB) + string(resB)

	return keyS
}

type BodyBytesPtr struct {
	Ptr *[]byte
}

func (bpr *BodyBytesPtr) MarshalJSON() ([]byte, error) {
	if bpr.Ptr != nil {
		var b []byte
		b = *bpr.Ptr
		rm, mErr := json.Marshal(string(b))
		if mErr != nil {
			panic(mErr)
		}
		return rm, nil
	} else {
		return json.Marshal(nil)
	}
}

func (bpr *BodyBytesPtr) UnmarshalJSON(b []byte) error {
	if string(b) == "null" {
		bpr.Ptr = nil
	} else {
		s, _ := strconv.Unquote(string(b))
		unescapedB := []byte(s)
		bpr.Ptr = &unescapedB
	}
	return nil
}

type TraceRecord struct {
	History
	ReducedTrace *ReducedTrace
}

type SpanRecord struct {
	History
	ReducedSpan *ReducedSpan
}

type History struct {
	Timestamps []time.Time
}

func (h *History) Update(reqTime, resTime time.Time) {
	h.Timestamps = append(h.Timestamps, reqTime, resTime)
}

type Reporter interface {
	Update(time.Time, time.Time)
	Report()
}
