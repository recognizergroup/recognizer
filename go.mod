module recognizer

go 1.22.1

require (
	github.com/PaesslerAG/jsonpath v0.1.1
	github.com/antchfx/xmlquery v1.4.1
	github.com/redis/go-redis/v9 v9.5.1
)

require (
	github.com/PaesslerAG/gval v1.0.0 // indirect
	github.com/antchfx/xpath v1.3.1 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	golang.org/x/net v0.7.0 // indirect
	golang.org/x/text v0.7.0 // indirect
)
