// Package doc provides a mock Depended-On-Component to demonstrate instrumented interactions with the System-Under-Test
package doc

import (
	"net/http"
)

var TestResponse = "DOC incoming Response.Body - raw text"

func RouteRequest(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		_, err := w.Write([]byte(TestResponse))
		if err != nil {
			panic(err)
		}
	} else if r.Method == "POST" {

	}
}
