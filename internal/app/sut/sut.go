// Package sut provides a mock System-Under-Test to demonstrate instrumented behavior
package sut

import (
	"encoding/json"
	"fmt"
	"github.com/antchfx/xmlquery"
	"io"
	"net/http"
	recognizer "recognizer/pkg/instrumentation"
	"strings"
)

var (
	docAddr = "http://localhost:4444"
)

// serviceLogic provides an example of how application code can use instrumented methods to interact with a DOC and handle errors
func serviceLogic(w http.ResponseWriter, r *http.Request) string {
	if r.Method == "GET" {
	} else if r.Method == "POST" {
		bodyBytes, readErr := io.ReadAll(r.Body)
		if readErr != nil {
			panic(readErr)
		}
		switch r.URL.Path {
		case "/":
			fmt.Printf("SUT calling DOC url: %s\n", docAddr)
			docResp, connectErr := recognizer.InstrumentedGet(docAddr)
			if connectErr != nil {
				recognizer.Error(w, connectErr)
			}
			fmt.Printf("DOC response status: %s\n", docResp.Status)
			docBody, docReadErr := io.ReadAll(docResp.Body)
			docCloseErr := docResp.Body.Close()
			if docCloseErr != nil {
				panic(docCloseErr)
			}
			if docReadErr != nil {
				recognizer.Error(w, docReadErr)
			}
			fmt.Printf("DOC Response.Body == \"%s\"\n", string(docBody))
			break
		case "/xmltest":
			doc, parseErr := xmlquery.Parse(strings.NewReader(string(bodyBytes)))
			if parseErr != nil {
				recognizer.Error(w, parseErr)
			}
			result, queryErr := xmlquery.QueryAll(doc, "/root/UserData/UserProperty")
			if queryErr != nil {
				recognizer.Error(w, queryErr)
			}
			if result != nil && result[0].InnerText() != "true" {
				recognizer.Error(w, fmt.Errorf("simulated application logic failure!"))
			}
		case "/jsontest":
			bodyInterface := map[string]interface{}{}
			jsonErr := json.Unmarshal(bodyBytes, &bodyInterface)
			if jsonErr != nil {
				recognizer.Error(w, jsonErr)
			}
			if val, ok := bodyInterface["UserProperty"]; ok && !val.(bool) {
				recognizer.Error(w, fmt.Errorf("simulated application logic failure!"))
			}
		}
		closeErr := r.Body.Close()
		if closeErr != nil {
			recognizer.Error(w, closeErr)
		}
	}
	return "OK"
}

// RouteRequest provides an example of how instrumentation can be embedded in a wrapper/framework layer
func RouteRequest(w http.ResponseWriter, r *http.Request) {
	recognizer.InstrumentRequest(r)
	body := serviceLogic(w, r)
	defer recognizer.ClearTrace(w.Header(), body)
	_, err := w.Write([]byte(body))
	if err != nil {
		panic(err)
	}
}
