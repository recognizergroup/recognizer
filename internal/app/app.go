package app

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"recognizer/internal/app/doc"
	"recognizer/internal/app/repository"
	"recognizer/internal/app/sut"
	engine "recognizer/pkg/repository"
	"strconv"
)

type ServerConfigs struct {
	Port   string
	Router RouteRequest
}
type RouteRequest func(w http.ResponseWriter, r *http.Request)

type SearchParams struct {
	SearchType string
	Key        string
	Route      string
	Start      string
	End        string
}

const (
	SUT        = "3333"
	DOC        = "4444"
	RECOGNIZER = "5555"
)

var (
	serverMap     = make(map[string]*http.Server)
	serverConfigs = map[string]ServerConfigs{
		"SUT (System-under-Test)":     {Port: SUT, Router: sut.RouteRequest},
		"Recognizer":                  {Port: RECOGNIZER, Router: engine.RouteRequest},
		"DOC (Depended-on-Component)": {Port: DOC, Router: doc.RouteRequest},
	}
)

func GetSUTRequest(method string, body string, path string) *http.Request {
	r, err := http.NewRequest(method, "http://localhost:"+SUT+"/"+path, bytes.NewBuffer([]byte(body)))
	if err != nil {
		panic(err)
	}
	return r
}

func GetRepoRequest(sp *SearchParams) *http.Request {
	Values := url.Values{}
	Url := url.URL{
		Scheme: "http",
		Host:   fmt.Sprintf("localhost:%s", RECOGNIZER),
	}
	if sp.SearchType == "span" || sp.SearchType == "trace" {
		Url.Path = sp.SearchType
	} else {
		Url.Path = "span"
	}
	if sp.Key != "" {
		Values["Hash"] = []string{sp.Key}
	}
	if sp.Route != "" {
		Values["Route"] = []string{sp.Route}
	}
	if sp.Start != "" {
		Values["Start"] = []string{sp.Start}
	}
	if sp.End != "" {
		Values["End"] = []string{sp.End}
	}
	Url.RawQuery = Values.Encode()
	urlS := Url.String()
	r, err := http.NewRequest("GET", urlS, nil)
	if err != nil {
		panic(err)
	}
	return r
}

func getHandleFunc(a string, f RouteRequest) *http.Server {
	newHandler := http.NewServeMux()
	newHandler.HandleFunc("/", f)

	return &http.Server{
		Addr:    ":" + a,
		Handler: newHandler,
	}
}

func StartAllServices() {
	_, cancelCtx := context.WithCancel(context.Background())
	repository.SetupRedisOnce()

	for k, v := range serverConfigs {
		serverName := k
		port := v.Port
		router := v.Router
		go func() {
			newHandler := getHandleFunc(port, router)
			fmt.Printf("created new service \"%s\" on port: %s\n", serverName, port)
			serverMap[serverName] = newHandler
			err := newHandler.ListenAndServe()
			if errors.Is(err, http.ErrServerClosed) {
				fmt.Printf("server closed\n")
			} else if err != nil {
				fmt.Printf("error listening for server: %s\n", err)
			} else {
				fmt.Printf("what's up now?")
			}
			cancelCtx()
		}()
	}

	go func() {
		fmt.Println("Waiting for servers to start...")
		for {
			if len(serverMap) >= len(serverConfigs) {
				fmt.Printf("All %s services started\n", fmt.Sprint(len(serverMap)))
				break
			}
		}
	}()

	cancelCtx()
}

func ShutdownAllServices(final bool) {
	repository.RedisClient.FlushAll(context.Background())
	if final {
		repository.TeardownRedisOnce()
	}
	for name, curServer := range serverMap {
		fmt.Printf("shutting down \"%s\"\n", name)
		thisServer := curServer
		go func() {
			err := thisServer.Shutdown(context.Background())
			if err != nil {
				panic(err)
			}
		}()
	}
	fmt.Printf("All %s services shutdown.\n", strconv.Itoa(len(serverMap)))
}
