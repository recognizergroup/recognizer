package repository

import "time"

const TimeStampLayout string = "2006-01-02 15:04:05.999999999 -0700 MST"

func IsTimely(t time.Time, start time.Time, end time.Time) bool {
	switch {
	case !start.IsZero() && !end.IsZero():
		return t.After(start) && t.Before(end) || t.Equal(start) || t.Equal(end)
	case start.IsZero() && !end.IsZero():
		return t.Before(end) || t.Equal(end)
	case !start.IsZero() && end.IsZero():
		return t.After(start) || t.Equal(start)
	}
	return false
}

func parseTime(s string) time.Time {
	startT, stParseErr := time.Parse(TimeStampLayout, s)
	if stParseErr != nil {
		panic(stParseErr)
	}
	return startT
}

func ParseTimes(start string, end string) (time.Time, time.Time) {
	var startT time.Time
	var endT time.Time

	if start != "" {
		startT = parseTime(start)
	}
	if end != "" {
		endT = parseTime(end)
	}
	return startT, endT
}
