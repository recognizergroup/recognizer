package repository

import (
	"context"
	"encoding/json"
	"github.com/redis/go-redis/v9"
	"os"
	"os/exec"
	"sync"
)

var (
	REDISCANCEL       *func() error
	RedisClient       *redis.Client
	teardownRedisOnce sync.Once
	setupRedisOnce    sync.Once
	DefaultRedisURL   = "localhost:6379"
)

func teardownRedis() {
	f := *REDISCANCEL
	redisErr := f()
	if redisErr != nil {
		panic(redisErr)
	}
}

func InsertRoute(key string, route string) {
	finalRoute := "ROUTE " + route
	keysStr := RedisClient.Get(context.Background(), finalRoute).Val()
	var keys []string
	if keysStr != "" {
		umErr := json.Unmarshal([]byte(keysStr), &keys)
		if umErr != nil {
			panic(umErr)
		}
	}
	keys = append(keys, key)
	b, mErr := json.Marshal(keys)
	if mErr != nil {
		panic(mErr)
	}
	RedisClient.Set(context.Background(), finalRoute, string(b), 0)
}

func GetRouteKeys(route string) []string {
	// TODO also wildcard match?
	keysStr := RedisClient.Get(context.Background(), "ROUTE "+route).Val()
	if keysStr == "" {
		return []string{}
	}
	var keys []string
	umErr := json.Unmarshal([]byte(keysStr), &keys)
	if umErr != nil {
		panic(umErr)
	}
	return keys
}

func TeardownRedisOnce() {
	teardownRedisOnce.Do(teardownRedis)
}
func setupRedis() {
	ctx, cancel := context.WithCancel(context.Background())
	cleanUpCmd := exec.Command("redis-cli", "shutdown")
	runErr := cleanUpCmd.Run()
	if runErr != nil {
		//panic(runErr)
	}
	cmd := exec.CommandContext(ctx, "redis-stack-server")
	f := func() error {
		cancel()
		return cmd.Cancel()
	}
	REDISCANCEL = &f
	if err := cmd.Start(); err != nil {
		panic(err)
	}
	if RedisClient == nil {
		var URL string
		if URL = os.Getenv("REDIS-URL"); URL == "" {
			URL = DefaultRedisURL
		}
		RedisClient = redis.NewClient(&redis.Options{
			Addr:     URL,
			Password: "", // no password set
			DB:       0,  // use default DB
		})
	}

	for { // waiting for redis to be truly ready
		pingErr := RedisClient.Ping(context.Background())
		if pingErr != nil {
			break
		}
	}
}
func SetupRedisOnce() {
	setupRedisOnce.Do(setupRedis)
}
