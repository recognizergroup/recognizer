package testing

import (
	"encoding/csv"
	"os"
	"testing"
)

func loadTestMemories(testName string) *Memories {
	f, osErr := os.Open(getMemoryPath() + testName + ".csv")
	var records [][]string
	var parseErr error
	if osErr == nil {
		cReader := csv.NewReader(f)
		records, parseErr = cReader.ReadAll()
		if parseErr != nil {
			panic(parseErr)
		}
		closeErr := f.Close()
		if closeErr != nil {
			panic(closeErr)
		}
	}
	m := make(Memories, func(r [][]string) int {
		if len(r) > 0 {
			return len(r) - 1
		} else {
			return 0
		}
	}(records))
	for index, record := range records {
		if index == 0 {
			continue
		}
		parseBool := func(s string) bool { return s == "true" }
		m[record[2]] = &Memory{
			TestName:  record[0],
			LastRun:   record[1],
			PassDescr: record[2],
			ActualS:   record[3],
			ExpectedS: record[4],
			Passed:    parseBool(record[5]),
			Regressed: parseBool(record[6]),
		}
	}

	return &m
}

func Setup(t *testing.T, postSetup func(), final ...bool) *HelpfulT {
	// test harness setup
	m := loadTestMemories(t.Name())
	var _final bool
	if len(final) == 0 {
		_final = false
	} else {
		_final = final[0]
	}
	ht := &HelpfulT{
		T:           t,
		Memories:    m,
		Final:       _final,
		Regressions: make([]int, 0),
	}

	// test harness start up
	postSetup()
	return ht
}
