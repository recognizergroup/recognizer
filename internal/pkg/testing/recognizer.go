package testing

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	engine "recognizer/pkg/repository"
	"testing"
)

func GetResponse[K engine.SpanResponseShape | engine.TraceResponseShape](t *testing.T, resp *http.Response) *K {
	bodyBytes, readErr := io.ReadAll(resp.Body)
	if readErr != nil {
		fmt.Println(readErr)
		t.Fail()
		panic(readErr)
	}
	var thing K
	jsonErr := json.Unmarshal(bodyBytes, &thing)
	if jsonErr != nil {
		fmt.Println(jsonErr)
		t.Fail()
		panic(jsonErr)
	}
	return &thing
}
