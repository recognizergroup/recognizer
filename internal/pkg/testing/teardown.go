package testing

import (
	"encoding/csv"
	"errors"
	"fmt"
	"os"
	"strconv"
	"time"
)

func Teardown(ht *HelpfulT, postTeardown func(bool)) {
	if ht.Memory.Closed {
		fmt.Println("*** Tearing down ***")
	} else {
		ht.Memory.Close(false)
		fmt.Printf("RUNTIME ERROR WHILE EXECUTING TEST: %s\n", ht.Memory.LineNo)
		fmt.Println("*** Tearing down EARLY!!! ***")
	}
	postTeardown(ht.Final)
	updateTestMemories(ht)
	fmt.Println("****** FINAL TEST RESULTS ******")
	fmt.Printf("at %s\n", time.Now().String())
	if ht.Passed == ht.TestCount {
		fmt.Printf("all %d tests passing!\n", ht.Passed)
	} else {
		fmt.Printf("only %d of %d tests passing!\n", ht.Passed, ht.TestCount)
		if len(ht.Regressions) > 0 {
			fmt.Printf("%d tests regressed this run: %v!\n", len(ht.Regressions), ht.Regressions)
		}
		ht.T.FailNow()
	}
	fmt.Println("********************************")
}

func (m *Memory) getCSV() []string {
	return []string{m.TestName, m.LastRun, m.PassDescr, m.ActualS, m.ExpectedS, strconv.FormatBool(m.Passed), strconv.FormatBool(m.Regressed)}
}

func updateTestMemories(ht *HelpfulT) {
	newPath := getMemoryPath()
	if _, err := os.Stat(newPath); errors.Is(err, os.ErrNotExist) {
		mkdirErr := os.Mkdir(newPath, 0755)
		if mkdirErr != nil {
			panic(mkdirErr)
		}
	}
	f, osErr := os.Create(newPath + ht.T.Name() + ".csv")
	if osErr != nil {
		panic(osErr)
	}
	csvW := csv.NewWriter(f)
	memories := *ht.Memories
	csvW.Write(CSVHEADINGS)
	for _, m := range memories {
		record := m.getCSV()
		writeErr := csvW.Write(record)
		if writeErr != nil {
			panic(writeErr)
		}
	}
	csvW.Flush()
	closeErr := f.Close()
	if closeErr != nil {
		panic(closeErr)
	}
}
