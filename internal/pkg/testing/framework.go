package testing

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/redis/go-redis/v9"
	"io"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"recognizer/internal/app/repository"
	engine "recognizer/pkg/instrumentation/reducer"
	"regexp"
	"runtime"
	"strings"
	"testing"
	"time"
)

var (
	PATHTEST    = "test"
	CSVHEADINGS = []string{"testName", "lastRun", "passDescr", "actualS", "expectedS", "passed", "regressed"}
)

type Memory struct {
	TestName      string
	ActualDescr   string
	ExpectedDescr string
	ActualS       string
	ExpectedS     string
	PassDescr     string
	Passed        bool
	Regressed     bool
	LastRun       string
	Closed        bool
	LineNo        string
}

type Memories map[string]*Memory

type HelpfulT struct {
	T           *testing.T
	Passed      int
	TestCount   int
	RunFile     string
	Regressions []int
	Final       bool
	*Memory
	*Memories
}

func prepend[K any](a []K, v ...K) []K {
	return append(v, a...)
}

func (ms *Memories) Open(actual interface{}, expected interface{}, terms ...string) *Memory {
	passDescr, actualDescr, expectedDescr := getStrings(actual, expected, terms...)
	actualS, expectedS := fmt.Sprintf("%v", actual), fmt.Sprintf("%v", expected)
	updatedMS := *ms
	m := updatedMS[passDescr]
	lineNo := getCodeLoc(4)
	if m == nil {
		m = &Memory{
			LineNo:        lineNo,
			ActualS:       actualS,
			ExpectedS:     expectedS,
			ActualDescr:   actualDescr,
			ExpectedDescr: expectedDescr,
			PassDescr:     passDescr,
		}
		updatedMS[passDescr] = m
		*ms = updatedMS
	} else {
		m.ActualDescr, m.ExpectedDescr = actualDescr, expectedDescr
	}
	return m
}

func getMemoryPath() string {
	wd, err := os.Getwd()
	if err != nil {
		panic(fmt.Errorf("%s could not get current working directory", err))
	}
	return path.Join(filepath.Dir(wd), PATHTEST) + "/"
}

func (m *Memory) Close(curPass bool) bool {
	m.Closed = true
	prevPass := m.Passed
	m.Passed = curPass
	m.LastRun = time.Now().String()
	if !prevPass && curPass {
		m.Regressed = false
		return true
	} else if !prevPass {
		m.Regressed = false
	} else if !curPass {
		m.Regressed = true
	} else {
		m.Regressed = false
	}
	return false
}

func getCodeLoc(skip int) string {
	_, file, line, _ := runtime.Caller(skip)
	s := strings.Split(file, "/")
	file = s[len(s)-1]
	return fmt.Sprintf("%s:%d", file, line)
}

func (ht *HelpfulT) Fail() {
	if ht.ActualS == ht.ExpectedS {
		ht.Pass() // fail-safe because this actually happens sometimes due to weird race conditions!
		return
	}
	ht.T.Fail()
	fmt.Printf("FAILED at: %s\n", getCodeLoc(3))
	fmt.Printf("Expected %s\n", ht.PassDescr)
	fmt.Printf("Got %v == %v\n", ht.ActualDescr, ht.ActualS)
	if ht.ExpectedDescr != ht.ExpectedS {
		fmt.Printf("Got %v == %v\n", ht.ExpectedDescr, ht.ExpectedS)
	}
	ht.Memory.Close(false)

	if ht.Regressed {
		fmt.Printf("REGRESSED!\n")
		ht.Regressions = append(ht.Regressions, ht.TestCount-1)
	}
	return
}

func (ht *HelpfulT) Pass() {
	ht.Passed++
	fmt.Printf("Passed\n%s\n", ht.PassDescr)
	fixed := ht.Memory.Close(true)
	if fixed {
		fmt.Println("FIXED")
	}
}

// getStrings takes two comparands, actual and expected, then an array of strings where
//
//	the first element is a string description for the value of actual and the second is for the value of expected
//	the
func getStrings(actual interface{}, expected interface{}, descriptions ...string) (Pass string, Actual string, Expected string) {
	actualDescr, expectedDescr := fmt.Sprintf("%v", actual), fmt.Sprintf("%v", expected)
	operator := "=="
	if len(descriptions) > 0 {
		operator = descriptions[0]
	}
	if len(descriptions) > 1 {
		actualDescr = descriptions[1]
	}
	if len(descriptions) > 2 {
		expectedDescr = descriptions[2]
	}
	passDescr := fmt.Sprintf("%s %s %s", actualDescr, operator, expectedDescr)
	return passDescr, actualDescr, expectedDescr
}

func (ht *HelpfulT) addAssertion(actual interface{}, expected interface{}, terms ...string) {
	ht.TestCount++

	m := ht.Memories.Open(actual, expected, terms...)
	ht.Memory = m
	ht.Memory.TestName = ht.T.Name()

	fmt.Printf("*** Test %d Results ***\n", ht.TestCount-1)
}

func (ht *HelpfulT) AssertEqual(actual interface{}, expected interface{}, terms ...string) {
	ht.addAssertion(actual, expected, prepend(terms, "==")...)
	if actual != expected {
		ht.Fail()
	} else {

		ht.Pass()
	}
}

func (ht *HelpfulT) AssertNotEqual(actual interface{}, expected interface{}, terms ...string) {
	ht.addAssertion(actual, expected, prepend(terms, "!=")...)
	if actual == expected {
		ht.Fail()
	} else {

		ht.Pass()
	}
}

type HTTPResponse struct {
	Response string
	Error    string
}

func getHTTPResponseStruct(r *http.Response, e error) HTTPResponse {
	actual, _ := json.Marshal(r)
	errStr := ""
	if e != nil {
		errStr = e.Error()
	}
	return HTTPResponse{
		Response: string(actual),
		Error:    errStr,
	}
}

func (ht *HelpfulT) AssertHTTPBehavior(request *http.Request, expectedResponse *http.Response, expectedError error, terms ...string) *http.Response {
	if expectedResponse == nil {
		expectedResponse = &http.Response{StatusCode: 200}
	}
	actual, err := http.DefaultClient.Do(request)
	actualStruct := getHTTPResponseStruct(actual, err)
	expectedStruct := getHTTPResponseStruct(expectedResponse, expectedError)
	ht.addAssertion(actualStruct, expectedStruct, prepend(terms, "==")...) // TODO the output looks a bit funky because expected is empty and actual has default vals
	if CompareHTTPResponse(actual, expectedResponse) && errors.Is(err, expectedError) {
		ht.Pass()
	} else {
		ht.Fail()
	}
	return actual
}

func (ht *HelpfulT) AssertResponseBody(request *http.Request, expectedResponse []byte, expectedError error, terms ...string) {

}

func GetNow() string {
	return time.Now().Format(repository.TimeStampLayout)
}

func CompareHTTPResponse(actual *http.Response, expected *http.Response) bool {
	switch {
	case expected.StatusCode != 0 && actual.StatusCode != expected.StatusCode:
		return false
	case expected.Body != nil && smartBodyCompare(actual.Body, expected.Body):
		return false
	case expected.Header != nil && smartHeaderCompare(actual.Header, expected.Header):
		return false
	default:
		return true
	}
}

func smartHeaderCompare(actual, expected http.Header) bool {
	for k, vv := range actual {
		if val, ok := expected[k]; ok {
			if !smartStringCompare(val[0], vv[0]) {
				return false
			}
		} else {
			return false
		}
	}
	for k, vv := range expected {
		if val, ok := actual[k]; ok {
			if !smartStringCompare(val[0], vv[0]) {
				return false
			}
		} else {
			return false
		}
	}
	return true
}

func smartBodyCompare(a io.ReadCloser, b io.ReadCloser) bool {
	aBytes, _ := io.ReadAll(a)
	bBytes, _ := io.ReadAll(b)
	return smartStringCompare(string(aBytes), string(bBytes))
}

func smartStringCompare(a string, b string) bool {
	for _, mask := range engine.Masks {
		if mask.MatchPatternType == "regex" {
			re := regexp.MustCompile(mask.MatchPattern)
			a = re.ReplaceAllString(a, mask.ReplaceString)
			b = re.ReplaceAllString(b, mask.ReplaceString)
		}
	}
	return a == b
}

func GetKeys(repo *redis.Client, pattern string) []string {
	return repo.Keys(context.Background(), pattern).Val()
}
