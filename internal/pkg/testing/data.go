package testing

var GenericJSON = `{
  "MessageId": "9FAD6D95-C07F-B828-B24F-9FC9282FD4F9",
  "UserId": 4321,
  "TimeStamp": "2050-11-11T11:11:11Z",
  "UserSecret": "4321's secret data oh no",
  "UserProperty": true
}`

var InsigVariantJSON = `{
  "MessageId": "02AD6911-C07F-482A-B24F-5FC9282FD4F0",
  "UserId": 4266,
  "TimeStamp": "2050-11-11T11:33:33Z",
  "UserSecret": "4266's secret data oh no",
  "UserProperty": true
}`

var SigVariantJSON = `{
  "MessageId": "625F6B21-C07F-4928-B24F-AFC9282FD4CC",
  "UserId": 3206,
  "TimeStamp": "2050-11-11T11:22:22Z",
  "UserSecret": "3206's secret data oh no",
  "UserProperty": false
}`

var FilteredJSON = `{
  "MessageId": "123e4567-e89b-12d3-a456-9AC7CBDCEE52",
  "UserId": 9999,
  "TimeStamp": "2001-01-01T00:00:00Z",
  "UserSecret": "**********",
  "UserProperty": true
}`

var GenericXML = `<?xml version="1.0" encoding="UTF-8"?>
<root MessageId="02AD6911-C07F-482A-B24F-5FC9282FD4F0">
<UserData>
<UserId>3206</UserId>
<Secret>3206's secret data oh no</Secret>
<UserProperty>true</UserProperty>
</UserData>
<TimeStamp>2050-11-11T11:22:22Z</TimeStamp>
</root>
`

var InsigVariantXML = `<?xml version="1.0" encoding="UTF-8"?>
<root MessageId="9FAD6D95-C07F-B828-B24F-9FC9282FD4F9">
<UserData>
<UserId>4321</UserId>
<Secret>4321's secret data oh no</Secret>
<UserProperty>true</UserProperty>
</UserData>
<TimeStamp>2050-11-11T11:11:11Z</TimeStamp>
</root>
`

var FilteredXML = `<?xml version="1.0" encoding="UTF-8"?><root MessageId="123e4567-e89b-12d3-a456-9AC7CBDCEE52"><UserData><UserId>9999</UserId><Secret>**********</Secret><UserProperty>true</UserProperty></UserData><TimeStamp>2001-01-01T00:00:00Z</TimeStamp></root>`
