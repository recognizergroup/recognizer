# Recognizer

Recognizer is a software pattern where an instrumented System-Under-Test (SUT) that is also
a service emits recordings of its IO with its Client, and various other Depended-On-Components (DOC),
and is able to identify if a given recording is functionally distinct from any known
reduced recordings. If so, then it is added to the Recognizer's persistent memory as a holotype,
and otherwise the holotype's history is updated with metadata on the latest instance of it.

## Proof-of-Concept

This POC is a golang implementation of the recognizer pattern. It demonstrates
the following capabilities:
* Recording I/O for asynchronous processing - DONE!
* Filtering according to schema or regex to derive a reduced recording
  * Regex - DONE!
  * JSON - DONE!
  * XML - DONE!
* Hashing and calling Span repository to see if it's new - DONE!
* Shipping reduced recording - DONE!
* Searching for reduced recordings in repository - DONE! 

## Instructions

1. clone repo
2. install redis stack and set $PATH to where redis-stack-server lives
3. cd into repo
4. `go test` to see what's working/broken
5. execute main.go to put in listening mode and use curl/postman to send traffic to http://localhost:3333
6. use redisinsight to monitor recognizer activity