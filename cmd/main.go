package main

import "recognizer/internal/app"

func main() {
	app.StartAllServices()
	select {}
}
