package main

import (
	"net/url"
	. "recognizer/internal/app"
	"recognizer/internal/app/doc"
	. "recognizer/internal/pkg/testing"
	engine "recognizer/pkg/repository"
	"testing"
	"time"
)

func setup(t *testing.T, final ...bool) *HelpfulT {
	return Setup(t, StartAllServices, final...)
}

func teardown(ht *HelpfulT) {
	Teardown(ht, ShutdownAllServices)
}

func TestPostBasic(t *testing.T) {
	ht := setup(t)
	defer teardown(ht)
	r := GetSUTRequest("POST", GenericJSON, "")

	// test code execution
	ht.AssertHTTPBehavior(r, nil, nil)

	// test assertions
	repo := engine.GetRepository()
	routeKeys := GetKeys(repo, engine.RouteKeyPattern)
	traceKeys := GetKeys(repo, engine.TraceKeyPattern)
	spanKeys := GetKeys(repo, engine.SpanKeyPattern)
	ht.AssertEqual(len(routeKeys), 2, "len(routeKeys)")
	ht.AssertEqual(len(traceKeys), 1, "len(traceKeys)")
	ht.AssertEqual(len(spanKeys), 2, "len(spanKeys)")
}

func TestSameSUTTwice(t *testing.T) {
	ht := setup(t)
	defer teardown(ht)
	req0 := GetSUTRequest("POST", GenericJSON, "")
	req1 := GetSUTRequest("POST", GenericJSON, "")
	repo := engine.GetRepository()

	//time0 := GetNow()
	ht.AssertHTTPBehavior(req0, nil, nil)
	time.Sleep(100 * time.Millisecond)

	traceKeys := GetKeys(repo, engine.TraceKeyPattern)
	spanKeys := GetKeys(repo, engine.SpanKeyPattern)
	ht.AssertEqual(len(traceKeys), 1, "len(traceKeys)")
	ht.AssertEqual(len(spanKeys), 2, "len(spanKeys)")

	ht.AssertHTTPBehavior(req1, nil, nil)

	traceKeys = GetKeys(repo, engine.TraceKeyPattern)
	spanKeys = GetKeys(repo, engine.SpanKeyPattern)
	ht.AssertEqual(len(traceKeys), 1, "len(traceKeys)")
	ht.AssertEqual(len(spanKeys), 2, "len(spanKeys)")
}

func TestDiffSUTPath(t *testing.T) {
	ht := setup(t)
	defer teardown(ht)
	req0 := GetSUTRequest("POST", GenericJSON, "")
	req1 := GetSUTRequest("POST", SigVariantJSON, "")
	repo := engine.GetRepository()

	//time0 := GetNow()
	ht.AssertHTTPBehavior(req0, nil, nil)
	time.Sleep(100 * time.Millisecond)

	traceKeys := GetKeys(repo, engine.TraceKeyPattern)
	spanKeys := GetKeys(repo, engine.SpanKeyPattern)
	ht.AssertEqual(len(traceKeys), 1, "len(traceKeys)")
	ht.AssertEqual(len(spanKeys), 2, "len(spanKeys)")

	//time1 := GetNow()
	ht.AssertHTTPBehavior(req1, nil, nil)

	traceKeys = GetKeys(repo, engine.TraceKeyPattern)
	spanKeys = GetKeys(repo, engine.SpanKeyPattern)
	ht.AssertEqual(len(traceKeys), 2, "len(traceKeys) ")
	ht.AssertEqual(len(spanKeys), 3, "len(spanKeys) ")
}

// TODO how to set up different doc response? maybe expose a method on DOC to change state?
func TestSameSUTDiffDocResp(t *testing.T) {
	ht := setup(t)
	defer teardown(ht)
	req0 := GetSUTRequest("POST", GenericJSON, "")
	req1 := GetSUTRequest("POST", GenericJSON, "")

	time0 := GetNow()
	ht.AssertHTTPBehavior(req0, nil, nil)

	time.Sleep(1000 * time.Millisecond)
	doc.TestResponse = "different response!"
	time1 := GetNow()
	ht.AssertHTTPBehavior(req1, nil, nil)

	repo := engine.GetRepository()
	traceKeys := GetKeys(repo, engine.TraceKeyPattern)
	spanKeys := GetKeys(repo, engine.SpanKeyPattern)
	ht.AssertEqual(len(traceKeys), 2, "len(traceKeys) ")
	ht.AssertEqual(len(spanKeys), 3, "len(spanKeys) ")

	v := url.Values{"Start": []string{time0}}
	spans := engine.SearchSpan(v)
	ht.AssertEqual(len(spans), 4, "len(repo.SearchSpan(url.Values{\"Start\": []string{time0}}))")
	v1 := url.Values{"Start": []string{time1}}
	spans1 := engine.SearchSpan(v1)
	ht.AssertEqual(len(spans1), 2, "len(repo.SearchSpan(url.Values{\"Start\": []string{time1}}))")
}

func TestSearch(t *testing.T) {
	ht := setup(t)
	defer teardown(ht)
	req0 := GetSUTRequest("POST", GenericJSON, "")

	ht.AssertHTTPBehavior(req0, nil, nil)

	time.Sleep(500 * time.Millisecond) // TODO find a better way to delay this?

	repo := engine.GetRepository()
	traceKeys := GetKeys(repo, engine.TraceKeyPattern)
	ht.AssertEqual(len(traceKeys), 1, "len(traceKeys) ")

	searchTraceReq := GetRepoRequest(&SearchParams{SearchType: "trace", Key: traceKeys[0]})
	resp := ht.AssertHTTPBehavior(searchTraceReq, nil, nil)
	traceResp := GetResponse[engine.TraceResponseShape](t, resp)
	ht.AssertEqual(len(traceResp.Traces), 1, "len(sutSpanRecords.Spans)")
	reducedTrace := traceResp.Traces[0].ReducedTrace
	parentKey := reducedTrace.Keys[0]
	childKey := reducedTrace.Keys[1]
	parentSpan := reducedTrace.Map[parentKey]
	childSpan := reducedTrace.Map[childKey]
	sutSpanRequestBody := string(parentSpan.Request.GetBody())
	ht.AssertEqual(sutSpanRequestBody, GenericJSON, "sutSpanResponseBody")

	docSpanResponseBody := string(childSpan.Response.GetBody())
	ht.AssertEqual(docSpanResponseBody, doc.TestResponse, "docSpanResponseBody")
}

func TestTwoSameSearch(t *testing.T) {
	ht := setup(t, true)
	defer teardown(ht)
	req0 := GetSUTRequest("POST", GenericJSON, "")
	req1 := GetSUTRequest("POST", GenericJSON, "")

	time0 := GetNow()
	ht.AssertHTTPBehavior(req0, nil, nil)

	time.Sleep(1000 * time.Millisecond)
	time1 := GetNow()
	ht.AssertHTTPBehavior(req1, nil, nil)

	// here we are establishing a baseline for number of keys of various types in redis
	repo := engine.GetRepository()
	allKeys := GetKeys(repo, "*")
	routeKeys := GetKeys(repo, engine.RouteKeyPattern)
	traceKeys := GetKeys(repo, engine.TraceKeyPattern)
	spanKeys := GetKeys(repo, engine.SpanKeyPattern)
	ht.AssertEqual(len(allKeys), 5, "len(allKeys) ")
	ht.AssertEqual(len(routeKeys), 2, "len(routeKeys) ")
	ht.AssertEqual(len(traceKeys), 1, "len(traceKeys) ")
	ht.AssertEqual(len(spanKeys), 2, "len(spanKeys) ")
	v := url.Values{"Start": []string{time0}}
	traces := engine.SearchTrace(v)
	ht.AssertEqual(len(traces), 2, "len(repo.SearchTrace(url.Values{\"Start\": []string{time0}}))")
	v1 := url.Values{"Start": []string{time1}}
	traces1 := engine.SearchTrace(v1)
	ht.AssertEqual(len(traces1), 1, "len(repo.SearchTrace(url.Values{\"Start\": []string{time1}}))")

	// repeating search test via API: /trace?Start=time0
	searchTraceReqGT := GetRepoRequest(&SearchParams{SearchType: "trace", Start: time0})
	resp := ht.AssertHTTPBehavior(searchTraceReqGT, nil, nil)
	tr := GetResponse[engine.TraceResponseShape](t, resp)
	ht.AssertNotEqual(tr, nil, "tr := getResponse[engine.TraceResponseShape](ht, resp); tr")
	ht.AssertEqual(len(tr.Traces), 2, "len(tr.Traces")
	traceKey := tr.Traces[0].ReducedTrace.Key
	ht.AssertNotEqual(traceKey, "", "tr.Traces[0].ReducedTrace.Key")

	// repeating search test via API: /trace?Hash=traceKey where traceKey is from the response above
	searchTraceReqTraceId := GetRepoRequest(&SearchParams{SearchType: "trace", Key: traceKey})
	searchTraceRespTraceId := ht.AssertHTTPBehavior(searchTraceReqTraceId, nil, nil)
	ht.AssertNotEqual(searchTraceRespTraceId.Body, nil, "searchTraceRespTraceId.Body")
	tr1 := GetResponse[engine.TraceResponseShape](t, searchTraceRespTraceId)
	ht.AssertNotEqual(tr1, nil, "tr1 := getResponse[engine.TraceResponseShape](ht, searchTraceRespTraceId); tr1")
	ht.AssertEqual(len(tr1.Traces), 1, "len(tr1.Traces")

	// repeating search test via API: /span?Start=time0;End=time1
	searchSpanReqTimeBoxed := GetRepoRequest(&SearchParams{Start: time0, End: time1})
	searchSpanRespTimeBoxed := ht.AssertHTTPBehavior(searchSpanReqTimeBoxed, nil, nil)
	ht.AssertNotEqual(searchSpanRespTimeBoxed.Body, nil, "searchSpanRespTimeBoxed.Body")
	sr := GetResponse[engine.SpanResponseShape](t, searchSpanRespTimeBoxed)
	ht.AssertNotEqual(sr, nil, "sr := getResponse[engine.SpanResponseShape](ht, resp); tr")
	ht.AssertEqual(len(sr.Spans), 2, "len(sr.Spans)")
	spanKey := sr.Spans[1].ReducedSpan.Key
	ht.AssertNotEqual(spanKey, "", "sr.Spans[0].ReducedSpan.Key")

	// repeating search test via API: /span?Hash=spanKey where spanKey is from above response
	searchSpanReqSpanKey := GetRepoRequest(&SearchParams{Key: spanKey})
	searchSpanRespSpanKey := ht.AssertHTTPBehavior(searchSpanReqSpanKey, nil, nil)
	ht.AssertNotEqual(searchSpanRespSpanKey.Body, nil, "searchSpanRespSpanKey.Body")
	sr1 := GetResponse[engine.SpanResponseShape](t, searchSpanRespSpanKey)
	ht.AssertNotEqual(sr1, nil, "sr1 := getResponse[engine.SpanResponseShape](ht, searchSpanRespSpanKey); sr1")
	ht.AssertEqual(len(sr1.Spans), 1, "len(sr1.Spans")
}

func TestJSONFilter(t *testing.T) {
	ht := setup(t)
	defer teardown(ht)
	req0 := GetSUTRequest("POST", GenericJSON, "jsontest")
	req1 := GetSUTRequest("POST", InsigVariantJSON, "jsontest")

	ht.AssertHTTPBehavior(req0, nil, nil)

	ht.AssertHTTPBehavior(req1, nil, nil)

	repo := engine.GetRepository()
	traceKeys := GetKeys(repo, engine.TraceKeyPattern)
	spanKeys := GetKeys(repo, engine.SpanKeyPattern)
	ht.AssertEqual(len(traceKeys), 1, "len(traceKeys) ")
	ht.AssertEqual(len(spanKeys), 1, "len(spanKeys) ")
	urlValue := url.Values{"Hash": []string{spanKeys[0]}}
	spanRecord := engine.SearchSpan(urlValue)[0]
	actual := spanRecord.ReducedSpan.Request.ReducedRecording.GetBody()
	ht.AssertEqual(string(actual), FilteredJSON)
}

func TestXMLFilter(t *testing.T) {
	ht := setup(t)
	defer teardown(ht)
	req0 := GetSUTRequest("POST", GenericXML, "xmltest")
	req1 := GetSUTRequest("POST", InsigVariantXML, "xmltest")

	ht.AssertHTTPBehavior(req0, nil, nil)

	ht.AssertHTTPBehavior(req1, nil, nil)

	repo := engine.GetRepository()
	traceKeys := GetKeys(repo, engine.TraceKeyPattern)
	spanKeys := GetKeys(repo, engine.SpanKeyPattern)
	ht.AssertEqual(len(traceKeys), 1, "len(traceKeys) ")
	ht.AssertEqual(len(spanKeys), 1, "len(spanKeys) ")
	urlValue := url.Values{"Hash": []string{spanKeys[0]}}
	spanRecord := engine.SearchSpan(urlValue)[0]
	actual := spanRecord.ReducedSpan.Request.ReducedRecording.GetBody()
	ht.AssertEqual(string(actual), FilteredXML)
}

//
//func TestError(t *testing.T) {
//	ht := setup(t)
//	defer teardown(ht)
//	req0 := GetSUTRequest("POST", GenericJSON, "jsontest")
//	req1 := GetSUTRequest("POST", SigVariantJSON, "jsontest")
//
//	ht.AssertHTTPBehavior(req0, nil, nil)
//
//	ht.AssertHTTPBehavior(req1, nil, nil)
//
//	repo := engine.GetRepository()
//	traceKeys := GetKeys(repo, engine.TraceKeyPattern)
//	spanKeys := GetKeys(repo, engine.SpanKeyPattern)
//	ht.AssertEqual(len(traceKeys), 2, "len(traceKeys) ")
//	ht.AssertEqual(len(spanKeys), 2, "len(spanKeys) ")
//	urlValue := url.Values{"Hash": []string{spanKeys[0]}}
//	spanRecord := engine.SearchSpan(urlValue)[0]
//	errors := spanRecord.ReducedSpan.Response.Errors
//	ht.AssertEqual(len(errors), 1, "len(actual) ")
//	ht.AssertEqual(errors[0].Error(), "simulated application logic failure!")
//}
